/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ville
 */
@WebServlet(name = "servlet", urlPatterns = {"/servlet"})
public class Servlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO: get a real API key
        String apiKey = "6d24a0f0409463caee11379a3a8bf49a";
        String apiSig = "353f130e9ba5dcd844486e88f3e8cd6b";
        URL apiUrl = getFlickrApiUrl(apiKey, apiSig, 10);

        InputStream inputStream = apiUrl.openStream();
        JsonParser parser = Json.createParser(inputStream);

        List<String> urlPartKeys = FlickrPhotoURL.getKeys();
        Map<String, String> urlParts = new HashMap<>();

        Event event;
        String currentKey;
        boolean inPhotosArray = false;

        List<FlickrPhoto> photos = new ArrayList<>();

        while (parser.hasNext()) {
            event = parser.next();
            
            switch (event) {
                case KEY_NAME:
                    currentKey = parser.getString();

                    if (inPhotosArray && urlPartKeys.contains(currentKey)) {
                        parser.next();
                        urlParts.put(currentKey, parser.getString());
                    } else if (currentKey.equals("photo")) {
                        inPhotosArray = true;
                    }
                    break;
                case END_ARRAY:
                    inPhotosArray = false;
                    break;
                case END_OBJECT:
                    if (inPhotosArray) {
                        photos.add(
                                new FlickrPhoto(FlickrPhotoURL.getURL(urlParts),
                                urlParts.get("title")));
                        urlParts.clear();
                    }
                    break;
            }
        }

        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Flickr photos</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Latest Flickr photos</h1>");
            photos.forEach(photo -> out.println(photo.getHTMLCaption()));
            out.println("</body>");
            out.println("</html>");
        }
    }

    private URL getFlickrApiUrl(String apiKey, String apiSig, int photoCount) throws MalformedURLException {
        return new URL("https://api.flickr.com/services/rest/" +
                "?method=flickr.photos.getRecent" +
                "&api_key=" + apiKey +
                "&per_page=" + photoCount +
                "&format=json&nojsoncallback=1" +
                "&api_sig=" + apiSig);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
