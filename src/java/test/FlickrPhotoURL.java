/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Ville
 */
public class FlickrPhotoURL {
    private static List<String> urlPartKeys = Arrays.
                asList("farm", "server", "id", "secret", "title");
    
    public static List<String> getKeys() {
        return urlPartKeys;
    }
    
    public static URL getURL(Map<String, String> parameters) throws MalformedURLException {
        if (parameters.keySet().containsAll(urlPartKeys)) {
            return new URL(String.format(
                    "https://farm%s.staticflickr.com/%s/%s_%s.jpg",
                    parameters.get("farm"),
                    parameters.get("server"),
                    parameters.get("id"),
                    parameters.get("secret")));
        } else {
            return new URL("");
        }
    }
}
