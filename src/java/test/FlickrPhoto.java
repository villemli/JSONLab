/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.net.URL;

/**
 *
 * @author Ville
 */
public class FlickrPhoto {
    private final URL url;
    private final String title;
    
    public FlickrPhoto(URL url, String caption) {
        this.url = url;
        this.title = caption;
    }

    public String getHTMLCaption() {
        return String.format("<figure>" +
                "<img src=\"%s\">" +
                "<figcaption>%s</figcaption>" +
                "</figure><br>",
                this.getUrl(), this.getTitle());
    }
    
    /**
     * @return the url
     */
    public URL getUrl() {
        return url;
    }

    /**
     * @return the caption
     */
    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Title: " + this.getTitle() + "\n" +
                "URL: " + this.getUrl();
    }
}
